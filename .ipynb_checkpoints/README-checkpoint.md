# Machine Learning on Microcontrollers - Evaluation

This repository contains multiple Jupyter notebooks which were used for the evaluation part of the master thesis [Evaluation and Deployment of Resource-Constrained Machine Learning on Embedded Devices](https://gitlab.ethz.ch/tec/public/students/ma_heim).

The notebooks are numbered and ordered similar to the structure of the evaluation chapter of the thesis. However, each notebook can be used on its own.
The Markdown cells within the notebooks are used for describing the different plots.

The resulting figures are imported as `.pdf` and `.tex` using `tikzplotlib`. The latter was used for the thesis, therefore the `.pdf` sometimes has unrendered LaTeX annotations.

## Installation

```
pip install jupyterlab
pip install -r requirements.txt
```


## Usage

```
jupyter lab
```

You will be automatically redirect to your browser where you can explore the notebooks with Jupyter lab.



## Relevant Repositories

This repository is part of the Master Thesis **Evaluation and Deployment of Resource-Constrained Machine Learning on Embedded Devices**.

- [Master Repository](https://gitlab.ethz.ch/tec/public/students/ma_heim/ML-on-MCU)
- [Toolchain implemented in Jupyter Notebooks and ARM Mbed](https://gitlab.ethz.ch/tec/public/students/ma_heim/ML-on-MCU_toolchain)
- [Evaluated Neural Network Models](https://gitlab.ethz.ch/tec/public/students/ma_heim/ml-on-mcu_models)
- [TensorFlow Lite Micro Mbed Project for Benchmarking Models with cmsis-nn](https://gitlab.ethz.ch/tec/public/students/ma_heim/tflu_benchmark-model_mbed_cmsis-nn)
- [TensorFlow Lite Micro Mbed Project for Benchmarking Models](https://gitlab.ethz.ch/tec/public/students/ma_heim/tflu_benchmark-model_mbed)


## License

*tbd*